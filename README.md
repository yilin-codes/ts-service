# ts-service

## 介绍
TypeScript后端Demo

## 软件架构
主要框架：Koa2 + Sequelize

## 使用说明
1. 下载依赖：`npm install`
2. 第一次运行时去掉src/index.ts中`await createDb()`的注释,创建数据库
3. 如果本地主机上有redis和postgresql,命令行终端输入`npm run dev`即可运行
4. 本地主机上没有redis和postgresql,保证Docker Desktop在正常运行中,命令行终端输入`npm run docker`运行

### 数据库同步
项目启动后，如需从Model文件同步数据库变更，命令行调用以下接口：
curl http://localhost:8888/sys/syncDb -d ''

### Model同步
如果是先创建表，需要根据表创建Model，命令行调用以下接口：
curl http://localhost:8888/sys/syncModel -d "tables=app_demo,app_user"
注：tables参数指定需要创建Model的表，多个用','隔开


