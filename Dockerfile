FROM node:16-alpine

ENV NODE_ENV docker

WORKDIR /usr/src/app

# copy app source
COPY . .

RUN npm install --unsafe-perm

RUN npm run build

EXPOSE 8888

ENTRYPOINT ["npm", "run"]
CMD ["start"]