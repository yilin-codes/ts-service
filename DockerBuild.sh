#!/bin/sh
docker image inspect liyl/ts-service:alpine >/dev/null 2>&1
if [ $? -eq 0 ];then
  docker rmi -f liyl/ts-service:alpine
fi
docker container inspect ts-service >/dev/null 2>&1
if [ $? -eq 0 ];then
  docker rm -f ts-service
fi
docker build -t liyl/ts-service:alpine .