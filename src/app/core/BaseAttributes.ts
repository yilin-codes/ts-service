export interface BaseAttributes extends Record<string, any> {
  index: number;
  oid: string;
  creatorId: string;
  updaterId: string;
  createdAt: Date;
  updatedAt: Date;
}
