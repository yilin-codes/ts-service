import cls from "cls-hooked";

const clsNamespace = cls.createNamespace("cls-namespace");

export default clsNamespace;
