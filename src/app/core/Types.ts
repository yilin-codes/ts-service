import { Model } from "sequelize-typescript";

export type Constructor<T> = new (...args: any[]) => T;

export type ModelType<T extends Model<T>> = Constructor<T> & T;

export type Parameter<T> = {
  [K in keyof T]: T[K] | T[K][] | Record<string, any> | undefined;
};
