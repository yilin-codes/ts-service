import logConfig from "../config/log";
import log4js, { getLogger } from "koa-log4";
import chalk from "chalk";

log4js.configure(logConfig);

export function Log(message: string) {
  getLogger(chalk.white("default")).info(chalk.white(message));
}

export function LogSql(sql: string, details?: any) {
  const sqlColorify = chalk.rgb(92, 182, 92);
  getLogger(sqlColorify("SQL")).info(sqlColorify(sql));
}
export function LogError(err: Error) {
  let logText = "";
  // 错误信息开始
  logText += "\n*************** error log start ***************\n";
  // 添加请求日志
  // 错误名称
  logText += `err name: ${err.name}\n`;
  // 错误信息
  logText += `err message: ${err.message}\n`;
  // 错误详情
  logText += `err stack: ${err.stack}\n`;
  // 错误信息结束
  logText += "*************** error log end ***************\n";
  getLogger("ERROR").error(chalk.redBright(logText));
}
