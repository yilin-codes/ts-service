import { Transaction } from "sequelize";
import { pgDb } from "../db";
import clsNamespace from "./cls";

export class TransactionManager {
  private static clsTransactionKey = "transaction";
  public static getCtxTransaction(): Transaction {
    return clsNamespace.get(this.clsTransactionKey);
  }
  public static async transaction<T>(
    autoCallback: (t: Transaction) => PromiseLike<T>
  ): Promise<T> {
    let transaction = this.getCtxTransaction();
    if (transaction) {
      // 如果上下文已存在事务，使用已有的事务，在父事务中处理提交、回滚
      const res = await autoCallback.call(this, transaction);
      return res;
    }
    transaction = await pgDb.transaction();
    clsNamespace.set(this.clsTransactionKey, transaction);
    try {
      const res = await autoCallback.call(this, transaction);
      await transaction.commit();
      return res;
    } catch (error) {
      await transaction.rollback();
      throw error;
    } finally {
      // 从cls中释放transaction
      clsNamespace.set(this.clsTransactionKey, undefined);
    }
  }
}
