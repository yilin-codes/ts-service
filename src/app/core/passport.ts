import bcrypt from "bcryptjs";
import passport from "koa-passport";
import * as LocalStrategy from "passport-local";
import { Op } from "sequelize";
import { STRATEGY_KEY } from "../constants/keys";
import UserService from "../service/busi/UserService";

passport.serializeUser(async (user: Record<string, any>, done) => {
  done(null, user.oid);
});
passport.deserializeUser(async (oid: string, done) => {
  const user = await UserService.findById(oid);
  done(null, user);
});
passport.use(
  STRATEGY_KEY.LOCAL_STRATEGY,
  new LocalStrategy.Strategy(
    {
      usernameField: "account",
      passwordField: "password",
    },
    async (username, password, done) => {
      try {
        const user = await UserService.findOne({
          [Op.or]: [
            {
              account: { [Op.eq]: username },
            },
            {
              phoneNumber: { [Op.eq]: username },
            },
            {
              email: { [Op.eq]: username },
            },
          ],
        });
        if (!user) {
          throw new Error("账号不存在");
        }
        if (!bcrypt.compareSync(password, user.hashedPassword)) {
          throw new Error("密码错误");
        }
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
  )
);
export default passport;
