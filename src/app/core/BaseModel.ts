import _ from "lodash";
import {
  AutoIncrement,
  BeforeBulkCreate,
  BeforeCreate,
  Column,
  DataType,
  Model,
  PrimaryKey,
} from "sequelize-typescript";
import { MAX_ID_LEN } from "../constants/length";
import NanoidUtil from "../utils/NanoidUtil";
import { BaseAttributes } from "./BaseAttributes";

export class BaseModel<EMA = BaseAttributes> extends Model<EMA> {
  protected _idNamespace = _.snakeCase(this.constructor.name);

  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  index: number;

  @Column(DataType.STRING(MAX_ID_LEN))
  oid: string;

  @Column(DataType.STRING(MAX_ID_LEN))
  creatorId: string;

  @Column(DataType.STRING(MAX_ID_LEN))
  updaterId: string;

  @BeforeCreate
  static initData(instance: BaseModel) {
    if (!instance.getDataValue("oid")) {
      instance.setDataValue(
        "oid",
        NanoidUtil.genId(`${instance._idNamespace}:`)
      );
    }
    instance.setDataValue("updaterId", instance.getDataValue("creatorId"));
  }

  @BeforeBulkCreate
  static bulkInitData(instances: BaseModel[]) {
    instances.forEach((instance) => {
      this.initData(instance);
    });
  }
}
