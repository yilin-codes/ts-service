import { Context } from "koa";
import Router from "koa-router";
import { METHOD } from "../constants/common";
import { METADATA_KEY } from "../constants/keys";
import { parseQueryString } from "../utils/functions";
import { appRouter } from "./router";
import { Constructor } from "./Types";

export interface RouterAttributes {
  /** 路由名称 */
  name: string;
  /** 路由说明 */
  desc: string;
  /** 路由路径 */
  path: string;
  /** 路由器 */
  router: Router<any, any>;
  /** 请求方式 */
  method: METHOD | string;
  /** 是否跳过鉴权 */
  skipAuth: boolean;
  /** 中间件 */
  middlewares: ((...args: any[]) => any)[];
  /** 处理方法 */
  handler: (...args: any[]) => any;
}

export type ControllerOptions = Partial<
  Omit<
    RouterAttributes,
    keyof { name: string; desc: string; method: string; handler: any }
  >
>;

export type RequestMapOptions = Partial<
  Omit<RouterAttributes, keyof { name: string; handler: any }>
> & {
  method: METHOD | string;
};

export type RequestMethodOptions = Omit<RequestMapOptions, "method">;

function Controller(target: Constructor<any>): void;
function Controller(options?: ControllerOptions): ClassDecorator;
function Controller(...args: any[]): any {
  const defaultOptions: ControllerOptions = {
    path: "/",
    router: appRouter,
    skipAuth: false,
    middlewares: [
      async (ctx: Context, next: (...args: any[]) => any) => {
        return next();
      },
    ],
  };
  if (args.length === 0 || typeof args[0] === "object") {
    return function (target: Constructor<any>) {
      Reflect.defineMetadata(
        METADATA_KEY.CONTROLLER_OPTIONS,
        { ...defaultOptions, ...args[0] },
        target
      );
    };
  } else if (typeof args[0] === "function") {
    Reflect.defineMetadata(
      METADATA_KEY.CONTROLLER_OPTIONS,
      defaultOptions,
      args[0]
    );
  } else {
    throw new Error("注解使用错误: Controller注解只能应用于类");
  }
}

function RequestMap(options: RequestMapOptions): MethodDecorator {
  return function (
    target: any,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<any>
  ) {
    const methodObj = descriptor?.value;
    if (!methodObj || typeof methodObj !== "function") {
      throw new Error(`注解使用错误: RequestMap注解只能应用于方法`);
    }
    Reflect.defineMetadata(
      METADATA_KEY.REQUEST_OPTIONS,
      options,
      target,
      propertyKey
    );
  };
}

function Get(
  target: any,
  propertyKey: string | symbol,
  descriptor: TypedPropertyDescriptor<any>
): void;
function Get(options?: RequestMethodOptions): MethodDecorator;
function Get(...args: any[]): any {
  const defaultMiddlewares = [
    async (ctx: Context, next: (...args: any[]) => any) => {
      ctx.parsedQuery = parseQueryString(ctx);
      return next(ctx);
    },
  ];
  if (args.length <= 1) {
    const mids = args[0]?.middlewares || [];
    return RequestMap({
      ...args[0],
      method: METHOD.GET,
      middlewares: [...mids, ...defaultMiddlewares],
    });
  } else {
    const methodObj = args[2]?.value;
    if (!methodObj || typeof methodObj !== "function") {
      throw new Error("注解使用错误: Get注解只能应用于方法");
    }
    Reflect.defineMetadata(
      METADATA_KEY.REQUEST_OPTIONS,
      { method: METHOD.GET, middlewares: defaultMiddlewares },
      args[0],
      args[1]
    );
  }
}

function Post(
  target: any,
  propertyKey: string | symbol,
  descriptor: TypedPropertyDescriptor<any>
): void;
function Post(options?: RequestMethodOptions): MethodDecorator;
function Post(...args: any[]): any {
  if (args.length <= 1) {
    return RequestMap({ ...args[0], method: METHOD.POST });
  } else {
    const methodObj = args[2]?.value;
    if (!methodObj || typeof methodObj !== "function") {
      throw new Error("注解使用错误: Post注解只能应用于方法");
    }
    Reflect.defineMetadata(
      METADATA_KEY.REQUEST_OPTIONS,
      { method: METHOD.POST },
      args[0],
      args[1]
    );
  }
}

function Put(
  target: any,
  propertyKey: string | symbol,
  descriptor: TypedPropertyDescriptor<any>
): void;
function Put(options?: RequestMethodOptions): MethodDecorator;
function Put(...args: any[]): any {
  if (args.length <= 1) {
    return RequestMap({ ...args[0], method: METHOD.PUT });
  } else {
    const methodObj = args[2]?.value;
    if (!methodObj || typeof methodObj !== "function") {
      throw new Error("注解使用错误: Put注解只能应用于方法");
    }
    Reflect.defineMetadata(
      METADATA_KEY.REQUEST_OPTIONS,
      { method: METHOD.PUT },
      args[0],
      args[1]
    );
  }
}

function Delete(
  target: any,
  propertyKey: string | symbol,
  descriptor: TypedPropertyDescriptor<any>
): void;
function Delete(options?: RequestMethodOptions): MethodDecorator;
function Delete(...args: any[]): any {
  if (args.length <= 1) {
    return RequestMap({ ...args[0], method: METHOD.DELETE });
  } else {
    const methodObj = args[2]?.value;
    if (!methodObj || typeof methodObj !== "function") {
      throw new Error("注解使用错误: Delete注解只能应用于方法");
    }
    Reflect.defineMetadata(
      METADATA_KEY.REQUEST_OPTIONS,
      { method: METHOD.DELETE },
      args[0],
      args[1]
    );
  }
}

export { Controller, RequestMap, Get, Post, Put, Delete };
