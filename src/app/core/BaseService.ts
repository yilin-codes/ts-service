import {
  BulkCreateOptions,
  CreateOptions,
  FindOptions,
  Op,
  TruncateOptions,
  UpdateOptions,
  WhereOptions,
} from "sequelize";
import { TransactionManager } from "./TransactionManager";
import { ModelType } from "./Types";

export interface Pagination extends Record<string, any> {
  current?: number;
  pageSize?: number;
  total?: number;
}

export interface EMAtttributesWithPagination<EMAttributes>
  extends Record<string, any> {
  data: EMAttributes[];
  pagination: Pagination;
}

export class BaseService<EMAttributes extends Record<string, any>> {
  protected _model: ModelType<any>;
  constructor(model: ModelType<any>) {
    this._model = model;
  }
  public async create(
    data: Partial<EMAttributes>,
    options?: CreateOptions<EMAttributes>
  ): Promise<EMAttributes> {
    const entityRes = await this._model.create(data, {
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    const entityJson = entityRes.toJSON() as EMAttributes;
    return entityJson;
  }
  public async createMany(
    list: Partial<EMAttributes>[],
    options: BulkCreateOptions<EMAttributes>
  ): Promise<EMAttributes[]> {
    const entityResList = await this._model.bulkCreate(list, {
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return entityResList.map(
      (entityRes: EMAttributes) => entityRes.toJSON() as EMAttributes
    );
  }
  public async update(
    data: Partial<EMAttributes>,
    where: WhereOptions<EMAttributes>,
    options?: UpdateOptions<EMAttributes>
  ): Promise<EMAttributes[]> {
    const updateOptions: UpdateOptions = { ...options, where };
    const updateRes = await this._model.update(data, {
      individualHooks: true,
      transaction: TransactionManager.getCtxTransaction(),
      ...updateOptions,
    });
    return updateRes[1].map(
      (entityRes: EMAttributes) => entityRes.toJSON() as EMAttributes
    );
  }

  public async updateById(
    data: Partial<EMAttributes>,
    oid: string,
    options?: UpdateOptions<EMAttributes>
  ): Promise<EMAttributes[]> {
    const updateOptions: UpdateOptions = { ...options, where: { oid } };
    const updateRes = await this._model.update(data, {
      individualHooks: true,
      transaction: TransactionManager.getCtxTransaction(),
      ...updateOptions,
    });
    return updateRes[1].map(
      (entityRes: EMAttributes) => entityRes.toJSON() as EMAttributes
    );
  }

  public async count(
    where: WhereOptions<EMAttributes>,
    options?: FindOptions<EMAttributes>
  ): Promise<number> {
    const count = await this._model.count({
      where,
      ...options,
      transaction: TransactionManager.getCtxTransaction(),
    });
    return count;
  }

  public async find(
    where: WhereOptions<EMAttributes>,
    options?: FindOptions<EMAttributes>
  ) {
    const list = await this._model.findAll({
      where,
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return list.map((item: EMAttributes) => item.toJSON() as EMAttributes);
  }

  public async findWithPage(
    where: WhereOptions<EMAttributes>,
    pagination: Pagination,
    options?: FindOptions<EMAttributes>
  ): Promise<EMAtttributesWithPagination<EMAttributes>> {
    const { pageSize = 10, current = 1 } = pagination;
    const listWithCount = await this._model.findAndCountAll({
      where,
      limit: pageSize,
      offset: (current - 1) * pageSize,
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return {
      data: listWithCount.rows.map(
        (item: EMAttributes) => item.toJSON() as EMAttributes
      ),
      pagination: {
        current,
        pageSize,
        total: listWithCount.count,
      },
    };
  }

  public async findOne(
    where: WhereOptions<EMAttributes>,
    options?: FindOptions<EMAttributes>
  ): Promise<EMAttributes | null> {
    const res = await this._model.findOne({
      where,
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return res ? (res.toJSON() as EMAttributes) : null;
  }

  public async findById(
    oid: string,
    options?: FindOptions<EMAttributes>
  ): Promise<EMAttributes | null> {
    const res = await this._model.findOne({
      where: { oid: { [Op.eq]: oid } },
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return res ? (res.toJSON() as EMAttributes) : null;
  }

  public async findByIds(
    oids: string[],
    options?: FindOptions<EMAttributes>
  ): Promise<EMAttributes[]> {
    const list = await this._model.findAll({
      where: { oid: oids },
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return list.map((item: EMAttributes) => item.toJSON() as EMAttributes);
  }

  public async remove(
    where: WhereOptions<EMAttributes>,
    options?: TruncateOptions<EMAttributes>
  ): Promise<number> {
    const res = await this._model.destroy({
      where,
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return res;
  }

  public async removeById(
    oid: string,
    options?: TruncateOptions<EMAttributes>
  ): Promise<boolean> {
    const res = await this._model.destroy({
      where: { oid: { [Op.eq]: oid } },
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return !!res;
  }

  public async removeByIds(
    oids: string[],
    options?: TruncateOptions<EMAttributes>
  ): Promise<number> {
    const res = await this._model.destroy({
      where: { oid: oids },
      transaction: TransactionManager.getCtxTransaction(),
      ...options,
    });
    return res;
  }
}
