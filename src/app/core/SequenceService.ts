import _ from "lodash";
import { redisClient } from "../db";

export default class SequenceService {
  /**
   * 使用redis获取命名空间下的下一个序列值
   * @param seqKey 序列的命名空间
   * @param len 需要返回的序列长度
   * @returns
   */
  public static async getNextVal(
    seqKey: string,
    len?: number
  ): Promise<string> {
    const redisKey = `RSEQ_${seqKey}`;
    redisClient.incr(redisKey);
    const serialNumber = await redisClient.get(redisKey);
    if (!serialNumber) {
      throw new Error("failed to get redis sequence");
    }
    if (len) {
      return _.padStart(serialNumber, len, "0");
    }
    return serialNumber;
  }
}
