import Router from "koa-router";

const appRouter = new Router({ prefix: "/app" }); // 业务接口,也是默认接口
const apiRouter = new Router({ prefix: "/api" }); // 功能接口,实现独立的特定的功能
const sysRouter = new Router({ prefix: "/sys" }); // 后管接口,系统开发人员使用
appRouter.get("/", async (ctx) => {
  ctx.body = "Hello App";
});
apiRouter.get("/", async (ctx) => {
  ctx.body = "Hello Api!";
});
sysRouter.get("/", async (ctx) => {
  ctx.body = "Hello Sys!";
});

export { appRouter, apiRouter, sysRouter };
