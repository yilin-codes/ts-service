import { getLogger } from "koa-log4";
import { TransactionManager } from "./TransactionManager";

const logger = getLogger("decorator");

export function Transactional(
  target: any,
  propertyName: string,
  descriptor: TypedPropertyDescriptor<(...args: any[]) => any>
): any {
  const methodObj = descriptor?.value;
  if (!methodObj || typeof methodObj !== "function") {
    throw new Error("注解使用错误: Transactional注解只能应用于方法");
  }
  logger.info("transaction decoratorMethod:", methodObj);
  descriptor.value = async function (...args: any[]) {
    const result = await TransactionManager.transaction(async () => {
      const res = await methodObj.apply(this, args);
      return res;
    });
    return result;
  };
}
