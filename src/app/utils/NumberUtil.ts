import _ from "lodash";
import { ROUND_MODE } from "../constants/common";

export default class NumberUtil {
  /**
   * @param value 原数值
   * @param precision 精度,即小数点后的位数,默认2
   * @param roundMode 舍入方式,请使用ROUND_MODE常量,默认四舍五入
   * @returns 按照指定精度和舍入方式舍入后的数值
   */
  public static setToFixed(
    value: number,
    precision = 2,
    roundMode = ROUND_MODE.HALF_UP
  ): number {
    const bridgeNumber = 10 ** _.toInteger(precision);
    if (roundMode === ROUND_MODE.UP) {
      return Math.ceil(value * bridgeNumber) / bridgeNumber;
    }
    if (roundMode === ROUND_MODE.DOWN) {
      return Math.floor(value * bridgeNumber) / bridgeNumber;
    }
    return Math.round(value * bridgeNumber) / bridgeNumber;
  }
}
