import { nanoid } from "nanoid";

export default class NanoidUtil {
  public static genId(prefix = ""): string {
    return `${prefix}${nanoid()}`;
  }
}
