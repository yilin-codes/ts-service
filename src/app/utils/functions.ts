import { ParameterizedContext, QueryParam } from "koa";
import _ from "lodash";
import qs from "qs";

export function parseQueryString(ctx: ParameterizedContext): QueryParam {
  const query = ctx.querystring;
  if (!query) {
    return { query: {} };
  }
  const queryObj: any = qs.parse(query);
  Object.keys(queryObj).forEach((key) => {
    if (Array.isArray(queryObj[key])) {
      queryObj[key] = queryObj[key].map((item: any) => JSON.parse(item));
    } else if (queryObj[key].includes("{")) {
      queryObj[key] = JSON.parse(queryObj[key]);
    }
  });
  return queryObj;
}

export function ResponseHandle(
  ctx: ParameterizedContext,
  data: any,
  errorMsg?: string
): void {
  if (_.isNil(data)) {
    throw new Error(errorMsg || "数据缺失");
  }
  if (_.isBoolean(data) && !data) {
    throw new Error(errorMsg || "操作失败");
  }
  if (data instanceof String) {
    ctx.response.body = {
      success: true,
      message: data,
    };
  } else if (data.pagination) {
    ctx.response.body = {
      success: true,
      data: data.data,
      pagination: data.pagination,
    };
  } else {
    ctx.response.body = {
      success: true,
      data,
    };
  }
}

export function GetKeyFromValue(obj: Record<any, any>, value: any): any {
  for (const key in obj) {
    if (obj[key] === value) {
      return key;
    }
  }
  return undefined;
}

/**
 * @param list 隐含树形结构的数组
 * @param 父节点id字段,一般使用默认即可
 * @returns 父节点id到子节点的一对多映射map
 */
export function GetParent2ChildMap<TreeAttributes extends Record<string, any>>(
  list: TreeAttributes[],
  parentKey = "poid"
): Map<string, TreeAttributes[]> {
  const parent2ChildMap = new Map<string, TreeAttributes[]>();
  list.forEach((item) => {
    let childs = parent2ChildMap.get(item[parentKey]);
    if (childs === undefined) {
      childs = [item];
      parent2ChildMap.set(item[parentKey], childs);
    } else {
      childs.push(item);
    }
  });
  return parent2ChildMap;
}

/**
 * @param list 隐含树形结构的数组
 * @param rootId 指定根节点id
 * @param param 指定关键字段名,一般使用默认即可
 * @param _parent2ChildMap 方法内部生成使用,无需传值
 * @returns 根节点树下所有id的数组(包括根节点id本身)
 */
export function GetSubIdsFromList<TreeAttributes extends Record<string, any>>(
  list: TreeAttributes[],
  rootId: string,
  { idKey = "oid", parentKey = "poid" } = {},
  _parent2ChildMap?: Map<string, TreeAttributes[]>
): string[] {
  let parent2ChildMap = _parent2ChildMap;
  if (!parent2ChildMap) {
    parent2ChildMap = GetParent2ChildMap(list, parentKey);
  }
  const resIds: string[] = [rootId];
  const childs = parent2ChildMap.get(rootId);
  if (childs) {
    childs.forEach((child) => {
      resIds.push(
        ...this.getSubIdsFromList(
          list,
          child[idKey],
          { idKey, parentKey },
          parent2ChildMap
        )
      );
    });
  }
  return resIds;
}

/**
 * @param list 隐含树形结构的数组
 * @param fieldPicker 指定需要的字段,默认为{},即保留全部字段
 * @param rootId 指定根节点id,默认为"",即最顶层
 * @param param 指定关键字段名,一般使用默认即可
 * @param _parent2ChildMap 方法内部生成使用,无需传值
 * @returns 树形结构的数组
 */
export function GetSubTreesFromList<TreeAttributes extends Record<string, any>>(
  list: TreeAttributes[],
  fieldPicker: Record<string, string> = {},
  rootId = "",
  { idKey = "oid", parentKey = "poid", childrenKey = "children" } = {},
  _parent2ChildMap?: Map<string, TreeAttributes[]>
): Record<string, any>[] {
  let parent2ChildMap = _parent2ChildMap;
  if (!parent2ChildMap) {
    parent2ChildMap = GetParent2ChildMap(list, parentKey);
  }
  const subRoots = parent2ChildMap.get(rootId);
  if (subRoots?.length) {
    const subTrees = subRoots.map((subRoot: Record<string, any>) => {
      const childs = this.getSubTreesFromList(
        list,
        fieldPicker,
        subRoot[idKey],
        { idKey, parentKey, childrenKey },
        parent2ChildMap
      );
      let treeAttr: Record<string, any> = {};
      const fieldKeys = Object.keys(fieldPicker);
      if (fieldKeys.length > 0) {
        fieldKeys.forEach((key) => {
          treeAttr[key] = subRoot[fieldPicker[key]];
        });
      } else {
        treeAttr = { ...subRoot };
      }
      if (childs.length > 0) {
        treeAttr[childrenKey] = childs;
      }
      return treeAttr;
    });
    return subTrees;
  }
  return [];
}
