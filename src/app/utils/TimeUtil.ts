import moment from "moment-timezone";

export default class TimeUtil {
  public static SECOND = 1000;
  public static MINUTE = 60 * this.SECOND;
  public static HOUR = 60 * this.MINUTE;
  public static DAY = 24 * this.HOUR;
  public static WEEK = 7 * this.DAY;
  public static convertDate2Str(date: Date, formatParam?: string): string {
    let format = formatParam;
    if (!format) {
      format = "yyyy-MM-DD HH:mm:ss";
    }
    return moment(date).format(format);
  }

  public static getStartOfDay(date: Date): Date {
    return moment(date).startOf("day").toDate();
  }

  public static add(date: Date, durationInSecond: number): Date {
    return moment(date).add(durationInSecond, "s").toDate();
  }
}
