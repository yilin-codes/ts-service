import Koa, { ParameterizedContext } from "koa";
import websockify from "koa-websocket";
import koaBody from "koa-body";
import moment from "moment-timezone";
import { LogError } from "./core/logs";
import session from "koa-session";
import path from "path";
import passport from "./core/passport";
import { getLogger } from "koa-log4";
import chalk from "chalk";

moment.tz.setDefault("Asia/Shanghai");
moment.locale("zh-cn");
process.env.TZ = "Asia/Shanghai";

const app = websockify(new Koa(), {});
app.use(
  koaBody({
    multipart: true,
    formidable: {
      uploadDir: path.resolve(__dirname, "../resource/upload"),
      keepExtensions: true,
      hash: "md5",
    },
  })
);
app.keys = ["ts-service"];
app.use(session(app));
app.use(passport.initialize());
app.use(passport.session());
app.use(async (ctx: ParameterizedContext, next: (...args: any[]) => any) => {
  try {
    // 记录请求接收的时刻
    const start = Date.now();
    // 记录请求日志
    getLogger(chalk.blueBright("Request")).info(
      chalk.blueBright(`${ctx.method} ${ctx.url}`)
    );
    // 进入到下一个中间件
    await next();
    // 记录请求回应的时刻
    const cost = Date.now() - start;
    // 记录响应日志
    getLogger(chalk.blueBright("Response")).info(
      chalk.blueBright(`${ctx.method} ${ctx.url} ${ctx.status} cost:${cost}ms`)
    );
  } catch (error: any) {
    // 记录异常日志
    LogError(error);
    ctx.app.emit("error", error, ctx);
  }
});
app.on("error", (err: any, ctx: ParameterizedContext) => {
  const message = err.message || "系统异常";
  ctx.response.status = err.statusCode || err.status || 200;
  ctx.response.body = {
    success: false,
    message,
  };
});

export default app;
