import { createClient } from "redis";
import { Sequelize } from "sequelize-typescript";
import config from "../config";
import path from "path";
import { LogSql } from "../core/logs";

const { dbConfig, redisUrl } = config;

const redisClient = createClient({
  url: redisUrl,
});
(async () => {
  redisClient.on("error", (err) => console.log("Redis Client Error", err));
  await redisClient.connect();
})();

const pgDb = new Sequelize({
  database: dbConfig.name,
  username: dbConfig.username,
  password: dbConfig.password,
  host: dbConfig.host,
  port: dbConfig.port,
  dialect: "postgres",
  timezone: "+08:00",
  logging: dbConfig.logging ? LogSql : false,
  pool: dbConfig.pool,
});
const models = [path.resolve(__dirname, "../model/**/*")];
pgDb.addModels(models);

export { pgDb, redisClient };
