import devConfig from "./profile/dev";
import { PoolOptions } from "sequelize";
import dockerConfig from "./profile/docker";

export type DbConfig = {
  host: string;
  port: number;
  name: string;
  username: string;
  password: string;
  logging: boolean;
  pool: PoolOptions;
};

export type AppConfig = {
  env: string;
  port: string;
  dbConfig: DbConfig;
  redisUrl: string;
};

const configs: AppConfig[] = [devConfig, dockerConfig];

const config =
  configs.find((config) => config.env === process.env.NODE_ENV?.trim()) ||
  devConfig;

export default config;
