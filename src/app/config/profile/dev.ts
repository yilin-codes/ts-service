import { AppConfig } from "..";

const devConfig: AppConfig = {
  env: "dev",
  port: "8888",
  dbConfig: {
    host: "127.0.0.1",
    port: 5432,
    name: "service_demo",
    username: "admin",
    password: "pg12345",
    pool: {
      min: 5,
      max: 20,
      idle: 10000,
    },
    logging: true,
  },
  redisUrl: "redis://@localhost:6379",
};

export default devConfig;
