import { AppConfig } from "..";

const dockerConfig: AppConfig = {
  env: "docker",
  port: "8888",
  dbConfig: {
    host: "postgresql",
    port: 5432,
    name: "pgdata",
    username: "admin",
    password: "pg12345",
    pool: {
      min: 5,
      max: 20,
      idle: 10000,
    },
    logging: true,
  },
  redisUrl: "redis://@redis:6379",
};

export default dockerConfig;
