import { Configuration } from "koa-log4";

const logConfig: Configuration = {
  appenders: {
    console: {
      type: "stdout",
    },
    app: {
      // 文件日志
      type: "dateFile",
      filename: "logs/app",
      pattern: "yyyy-MM-dd.log",
      alwaysIncludePattern: true,
    },
    err: {
      // 错误日志
      type: "dateFile",
      filename: "logs/err",
      pattern: "yyyy-MM-dd.log",
      alwaysIncludePattern: true,
    },
    errFilter: {
      type: "logLevelFilter",
      level: "ERROR",
      appender: "err",
    },
  },
  categories: {
    default: { appenders: ["console", "app", "errFilter"], level: "info" },
  },
};

export default logConfig;
