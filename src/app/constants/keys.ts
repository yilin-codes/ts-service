export const METADATA_KEY = {
  CONTROLLER_OPTIONS: "controllerOptions",
  REQUEST_OPTIONS: "requestOptions",
};

export const STRATEGY_KEY = {
  LOCAL_STRATEGY: "local",
};
