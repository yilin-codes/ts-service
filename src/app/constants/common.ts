export enum METHOD {
  GET = "get",
  POST = "post",
  DELETE = "delete",
  PUT = "put",
}

export const WHETHER_FLAG = {
  NOT: "0",
  YES: "1",
};

export const USER_TYPE = {
  TEMP: "0",
  NORMAL: "1",
  ADMIN: "9",
};

export const ROUND_MODE = {
  HALF_UP: "0",
  UP: "1",
  DOWN: "2",
};
