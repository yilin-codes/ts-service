export const MAX_ID_LEN = 64;
export const MAX_CODE_LEN = 32;
export const MAX_FLAG_LEN = 4;
