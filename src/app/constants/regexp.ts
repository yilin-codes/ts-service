export const ACCOUNT_REGEXP = /[a-z0-9_]+/i;
export const EMAIL_REGEXP =
  /^([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{1,10}){0,2})+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,10}){1,2})$/im;
export const PHONE_REGEXP = /^1\d{10}$/i;
