import { ParameterizedContext } from "koa";
import { getLogger } from "koa-log4";
import { Controller, Get } from "../core/Controller";
import { sysRouter } from "../core/router";
import { ResponseHandle } from "../utils/functions";
import RouterService from "../service/base/RouterService";

@Controller({ router: sysRouter, path: "/router" })
export default class RouterController {
  private logger = getLogger(this.constructor.name);

  @Get
  public async getRouterList(ctx: ParameterizedContext): Promise<void> {
    const { query, pagination = {} } = ctx.parsedQuery;
    const res = RouterService.getRouterList(query, pagination);
    ResponseHandle(ctx, res);
  }
}
