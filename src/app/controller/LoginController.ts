import { ParameterizedContext } from "koa";
import { STRATEGY_KEY } from "../constants/keys";
import { Controller, Get, Post } from "../core/Controller";
import passport from "../core/passport";
import { ResponseHandle } from "../utils/functions";

@Controller
export default class LoginController {
  @Post({ path: "/login", desc: "用户登入接口", skipAuth: true })
  public async localLogin(ctx: ParameterizedContext, next: any) {
    await passport.authenticate(
      STRATEGY_KEY.LOCAL_STRATEGY,
      {
        failureRedirect: "/login",
        failureFlash: "incorrect account or password",
      },
      async (err: Error | null, user: Record<string, any>) => {
        if (err) {
          throw err;
        }
        await ctx.login(user);
        ResponseHandle(ctx, user);
        await next();
      }
    )(ctx, next);
  }

  @Get({ path: "/logout", desc: "用户登出接口", skipAuth: true })
  public async logout(ctx: ParameterizedContext) {
    const reqt: Record<string, any> = ctx.req;
    const user = reqt.user;
    if (!user) {
      ResponseHandle(ctx, null, "用户尚未登录，无法登出");
    }
    ctx.logout();
    ResponseHandle(ctx, `用户${user.username}已登出`);
  }
}
