import { ParameterizedContext } from "koa";
import { getLogger } from "koa-log4";
import { Controller, Post } from "../core/Controller";
import { ResponseHandle } from "../utils/functions";
import UserService from "../service/busi/UserService";

@Controller({ path: "/user" })
export default class UserController {
  private logger = getLogger(this.constructor.name);

  @Post
  public async create(ctx: ParameterizedContext): Promise<void> {
    this.logger.info("create user begin");
    const data = ctx.request.body;
    const res = await UserService.create(data);
    ResponseHandle(ctx, res, "创建用户失败");
    this.logger.info("create user end");
  }
}
