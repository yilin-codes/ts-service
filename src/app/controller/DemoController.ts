import { ParameterizedContext } from "koa";
import { getLogger } from "koa-log4";
import { Controller, Delete, Get, Post } from "../core/Controller";
import SequenceService from "../core/SequenceService";
import { ResponseHandle } from "../utils/functions";
import DemoService from "../service/busi/DemoService";

@Controller({ path: "/demo" })
export default class DemoController {
  private logger = getLogger(this.constructor.name);

  @Post
  public async create(ctx: ParameterizedContext): Promise<void> {
    this.logger.info("create demo begin");
    const data = ctx.request.body;
    if (!data.code) {
      data.code = await SequenceService.getNextVal("demo", 4);
    }
    const res = await DemoService.create(data);
    ResponseHandle(ctx, res, "创建Demo失败");
    this.logger.info("create demo end");
  }

  @Get
  public async searchList(ctx: ParameterizedContext): Promise<void> {
    const { query, pagination = {}, options } = ctx.parsedQuery;
    const res = await DemoService.findWithPage(query, pagination, options);
    ResponseHandle(ctx, res, "查询Demo失败");
  }

  @Delete({ path: "/:oid" })
  public async deleteDemo(ctx: ParameterizedContext) {
    const { oid } = ctx.params;
    const res = await DemoService.removeById(oid);
    ResponseHandle(ctx, res, "删除失败");
  }
}
