import LoginController from "./LoginController";
import UserController from "./UserController";
import DemoController from "./DemoController";
import DbController from "./DbController";
import RouterController from "./RouterController";
import RouterService from "../service/base/RouterService";

const Controllers = [
  DbController,
  RouterController,
  LoginController,
  UserController,
  DemoController,
];
const RegisterControllers = () => {
  RouterService.register(Controllers);
};
export default RegisterControllers;
