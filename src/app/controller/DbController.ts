import { ParameterizedContext } from "koa";
import { getLogger } from "koa-log4";
import _ from "lodash";
import { Controller, Get, Post } from "../core/Controller";
import { sysRouter } from "../core/router";
import { ResponseHandle } from "../utils/functions";
import DbService from "../service/base/DbService";
import { pgDb } from "../db";
import { QueryTypes } from "sequelize";

@Controller({ router: sysRouter, skipAuth: true })
export default class DbController {
  private logger = getLogger(this.constructor.name);

  @Post({ path: "/syncDb", desc: "根据Model同步生成或修改数据库表" })
  public async syncDb(ctx: ParameterizedContext) {
    await DbService.syncDb();
    ResponseHandle(ctx, "ok");
  }

  @Post({ path: "/syncModel", desc: "根据数据库中已有表生成Model文件" })
  public async syncModel(ctx: ParameterizedContext) {
    const {
      dir = "/src/genModels",
      tableNames,
      emptyDir = false,
    } = ctx.request.body;
    await DbService.syncModel(dir, tableNames, emptyDir);
    ResponseHandle(ctx, "ok");
  }

  @Get({ path: "/tableOptions" })
  public async searchTableOptions(ctx: ParameterizedContext) {
    const {
      query: { keywords },
    } = ctx.parsedQuery;
    const tableDefinitions = await DbService.getTableDefinitions();
    const res = [];
    if (_.isEmpty(keywords)) {
      res.push(
        ...tableDefinitions.map((t) => ({
          label: t.tableName,
          value: t.tableName,
        }))
      );
    } else {
      const tables = tableDefinitions.filter((tableDefinition) => {
        return tableDefinition.tableName.indexOf(keywords) > -1;
      });
      res.push(
        ...tables.map((t) => ({ label: t.tableName, value: t.tableName }))
      );
    }
    ResponseHandle(ctx, _.chunk(res, 20)[0]);
  }

  @Get({ path: "/tableFields" })
  public async getTableFields(ctx: ParameterizedContext) {
    const { query, pagination = {} } = ctx.parsedQuery;
    const { tableName } = query;
    const fieldInfo = await DbService.getTableFieldInfo(tableName, pagination);
    ResponseHandle(ctx, fieldInfo);
  }

  @Post({ path: "/setComment" })
  public async setComment(ctx: ParameterizedContext) {
    const { table, field, comment } = ctx.request.body;
    const res = await pgDb.query(
      `COMMENT ON COLUMN "public"."${table}"."${field}" IS '${comment}';`,
      { type: QueryTypes.UPDATE }
    );
    ResponseHandle(ctx, res);
  }
}
