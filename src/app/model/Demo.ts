import { Column, DataType, Table } from "sequelize-typescript";
import { MAX_CODE_LEN } from "../constants/length";
import { BaseAttributes } from "../core/BaseAttributes";
import { BaseModel } from "../core/BaseModel";

export interface DemoAttributes extends BaseAttributes {
  code: string;
  name: string;
}

@Table({
  underscored: true,
  tableName: "app_demo",
  indexes: [
    {
      unique: true,
      fields: ["code"],
    },
  ],
})
export class Demo extends BaseModel<DemoAttributes> {
  @Column(DataType.STRING(MAX_CODE_LEN))
  code: string;

  @Column
  name: string;
}
