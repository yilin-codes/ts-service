import bcrypt from "bcryptjs";
import {
  Column,
  DataType,
  Default,
  Table,
  Validate,
} from "sequelize-typescript";
import { USER_TYPE } from "../constants/common";
import { MAX_FLAG_LEN } from "../constants/length";
import { ACCOUNT_REGEXP, PHONE_REGEXP } from "../constants/regexp";
import { BaseAttributes } from "../core/BaseAttributes";
import { BaseModel } from "../core/BaseModel";

export interface UserAttributes extends BaseAttributes {
  account: string;
  username: string;
  password: string;
  hashedPassword: string;
  phoneNumber: string;
  email: string;
  type: string;
  lastLoginAt: Date;
}

@Table({
  underscored: true,
  tableName: "app_user",
  indexes: [
    {
      unique: true,
      fields: ["account"],
    },
  ],
})
export class User extends BaseModel<Partial<UserAttributes>> {
  @Validate({
    is: { msg: "账号只能是字母、数字与下划线的组合", args: ACCOUNT_REGEXP },
  })
  @Column
  account: string;

  @Column
  username: string;

  @Column({ type: DataType.STRING, allowNull: true })
  get password(): string {
    return this.getDataValue("password") as string;
  }

  set password(value: string) {
    this.setDataValue("password", value);
    this.setDataValue("hashedPassword", bcrypt.hashSync(value, 10));
  }

  @Column
  hashedPassword: string;

  @Validate({
    is: { msg: "请输入正确的手机号", args: PHONE_REGEXP },
  })
  @Column(DataType.STRING(11))
  phoneNumber: string;

  @Validate({
    isEmail: { msg: "请输入正确的邮箱" },
  })
  @Column
  email: string;

  @Default(USER_TYPE.NORMAL)
  @Column(DataType.STRING(MAX_FLAG_LEN))
  type: string;

  @Column
  lastLoginAt: Date;
}
