import { BaseService } from "../../core/BaseService";
import { Demo, DemoAttributes } from "../../model/Demo";

class DemoService extends BaseService<DemoAttributes> {}

export default new DemoService(Demo);
