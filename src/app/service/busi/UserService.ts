import { BaseService } from "../../core/BaseService";
import { User, UserAttributes } from "../../model/User";

class UserService extends BaseService<UserAttributes> {}

export default new UserService(User);
