import path from "path";
import fs from "fs";
import Automate from "sequelize-automate";
import config from "../../config";
import { Log } from "../../core/logs";
import { pgDb } from "../../db";
import _ from "lodash";
import { Client } from "pg";
import {
  EMAtttributesWithPagination,
  Pagination,
} from "../../core/BaseService";
import { User } from "../../model/User";
import { USER_TYPE } from "../../constants/common";

class DbService {
  /** 创建数据库 */
  public async createDb() {
    Log("createDb begin");
    const { dbConfig } = config;
    const pgClient = new Client({
      host: dbConfig.host,
      port: dbConfig.port,
      database: "postgres",
      user: dbConfig.username,
      password: dbConfig.password,
    });
    try {
      await pgClient.connect();
      const hasDb =
        (
          await pgClient.query(
            `SELECT datname FROM pg_database where datname = '${dbConfig.name}'`
          )
        ).rowCount > 0;
      if (!hasDb) {
        await pgClient.query(`CREATE DATABASE ${dbConfig.name}`);
      } else {
        Log(`database ${dbConfig.name} already exist`);
      }
    } catch (error: any) {
      throw new Error(`[createDb error]: ${error.message}`);
    } finally {
      await pgClient.end();
      Log("createDb end");
    }
  }

  /** 初始化管理员账号 */
  public async initAdmin() {
    const existAdmin = (
      await User.findOne({
        where: {
          account: "admin",
          type: USER_TYPE.ADMIN,
        },
      })
    )?.toJSON();
    if (_.isEmpty(existAdmin)) {
      await User.create({
        account: "admin",
        username: "admin",
        password: "password",
        type: USER_TYPE.ADMIN,
      });
    }
  }

  /**
   * 根据Model同步生成或修改数据库表
   */
  public async syncDb() {
    Log("syncDb begin");
    await pgDb.authenticate();
    await pgDb.sync({
      force: false,
      alter: true,
    });
    Log("syncDb end");
  }

  /**
   * 根据数据库中已有表生成Model文件
   * @param dir 文件目录
   * @param tableNames 指点要生成Model文件的表名,不传或传undefined时默认全部表
   * @param emptyDir 生成文件前是否先清空目录,默认不清空
   */
  public async syncModel(dir: string, tableNames?: string[], emptyDir = false) {
    Log("syncModel begin");
    const definitions = await this.getTableDefinitions(tableNames);
    await this.__createModelFile(dir, definitions, emptyDir);
    Log("syncModel end");
  }

  /**
   * 获取数据库表定义
   * @param tableNames 表名
   * @returns 表定义
   */
  public async getTableDefinitions(
    tableNames?: string[]
  ): Promise<Automate.Definition[]> {
    const { dbConfig } = config;
    const dbOptions: Automate.DbOptions = {
      database: dbConfig.name,
      username: dbConfig.username,
      password: dbConfig.password,
      dialect: "postgres",
      host: dbConfig.host,
      port: dbConfig.port,
      logging: false,
    };
    const options: Automate.Options = {
      type: "ts",
      camelCase: true,
      fileNameCamelCase: true,
      tables: tableNames ?? null,
      skipTables: null,
    };
    const automate = new Automate(dbOptions, options);
    const definitions = await automate.getDefinitions();
    return definitions;
  }

  public async getTableFieldInfo(
    tableName: string,
    pagination: Pagination
  ): Promise<EMAtttributesWithPagination<Automate.FieldAttr>> {
    if (_.isEmpty(tableName)) {
      return { data: [], pagination };
    }
    const tableDefinition = (await this.getTableDefinitions([tableName]))[0];
    const fields = Object.values(tableDefinition.attributes).map((field) => {
      return {
        ...field,
        type: field.type.substring("DataTypes.".length),
      };
    });
    const fieldss = _.chunk(fields, pagination.pageSize);
    return {
      data: fieldss[_.toInteger(pagination.current) - 1],
      pagination: { ...pagination, total: fields.length },
    };
  }

  private async __createModelFile(
    dir: string,
    definitions: Automate.Definition[],
    emptyDir: boolean
  ) {
    const dirDepth = __dirname
      .substring(__dirname.indexOf("src"))
      .split(path.sep).length;
    const filePath = path.resolve(__dirname, `${"../".repeat(dirDepth)}${dir}`);
    if (emptyDir && fs.existsSync(filePath)) {
      fs.rmSync(filePath, { recursive: true, force: true });
    }
    if (!fs.existsSync(filePath)) {
      fs.mkdirSync(filePath);
    }
    definitions.forEach((definition) => {
      const modelName = _.upperFirst(definition.modelName).replace("Model", "");
      const attrName = `${modelName}Attributes`;
      const modelFileName = _.upperFirst(definition.modelFileName);
      const fileName = `${filePath}/${modelFileName}.ts`;
      let data = `import { Table, Column, Model } from "sequelize-typescript";\n\n`;
      data = `${data}export interface ${attrName} extends Record<string, any> {\n`;
      const attrKeys = Object.keys(definition.attributes).filter(
        (key) => key !== "id"
      );
      const key2DefStrMap = new Map<string, string>();
      attrKeys.forEach((key) => {
        const fieldAttr = definition.attributes[key];
        const mark = fieldAttr.allowNull ? "?" : "";
        const typeStr = this.__getType(fieldAttr.type);
        key2DefStrMap.set(key, `  ${key}${mark}: ${typeStr};`);
        data = `${data}${key2DefStrMap.get(key)}\n`;
      });
      data = `${data}}\n\n`;
      data = `${data}@Table({ tableName: "${definition.tableName}" })\n`;
      data = `${data}class ${modelName} extends Model {`;
      const modelKeys = attrKeys.filter(
        (key) => key !== "createdAt" && key !== "updatedAt"
      );
      modelKeys.forEach((key) => {
        data = `${data}\n  @Column\n`;
        data = `${data}${key2DefStrMap.get(key)}\n`;
      });
      data = `${data}}\n\nexport { ${modelName} };\n`;
      fs.writeFileSync(fileName, data);
    });
  }

  private __getType(typeDefStr: string): string {
    const typeMap: Record<string, string> = {
      BOOLEAN: "boolean",
      INTEGER: "number",
      BIGINT: "number",
      STRING: "string",
      CHAR: "string",
      REAL: "number",
      TEXT: "string",
      DATE: "Date",
      FLOAT: "number",
      DECIMAL: "number",
      DOUBLE: "number",
      UUIDV4: "string",
    };
    const index = typeDefStr.indexOf("(");
    const key =
      index === -1 ? typeDefStr.slice(10) : typeDefStr.slice(10, index);
    const typeStr = typeMap[key];
    if (typeStr === undefined) {
      throw new Error(`没有给${key}映射类型`);
    }
    return typeStr;
  }
}

export default new DbService();
