import chalk from "chalk";
import { Context } from "koa";
import { getLogger } from "koa-log4";
import _ from "lodash";
import { METADATA_KEY } from "../../constants/keys";
import {
  EMAtttributesWithPagination,
  Pagination,
} from "../../core/BaseService";
import {
  ControllerOptions,
  RequestMapOptions,
  RouterAttributes,
} from "../../core/Controller";
import { Constructor } from "../../core/Types";

class RouterService {
  private _logger = getLogger(chalk.blueBright(this.constructor.name));
  private _routerMap: Record<string, RouterAttributes> = {};

  /**
   * 注册路由
   * @param controllers 控制类列表
   */
  public register(controllers: Constructor<any>[]) {
    controllers.forEach((controller) => {
      const controllerOptions: Required<ControllerOptions> =
        Reflect.getMetadata(METADATA_KEY.CONTROLLER_OPTIONS, controller);
      const controllerInstance: any = new controller();
      Object.getOwnPropertyNames(controller.prototype).forEach((methodName) => {
        const requestOptions: RequestMapOptions = Reflect.getMetadata(
          METADATA_KEY.REQUEST_OPTIONS,
          controllerInstance,
          methodName
        );
        if (requestOptions) {
          const routerAttr: RouterAttributes = {
            name: `${controller.name}.${methodName}`,
            desc: requestOptions.desc ?? "",
            path: _.replace(
              requestOptions.path
                ? controllerOptions.path + requestOptions.path
                : controllerOptions.path,
              /\/+/g,
              "/"
            ),
            router: requestOptions.router
              ? requestOptions.router
              : controllerOptions.router,
            method: requestOptions.method.toLowerCase(),
            skipAuth: requestOptions.skipAuth
              ? requestOptions.skipAuth
              : controllerOptions.skipAuth,
            middlewares: [
              ...controllerOptions.middlewares,
              ...(requestOptions.middlewares || []),
            ],
            handler: controllerInstance[methodName].bind(controllerInstance),
          };
          this.__addRouter(routerAttr);
        }
      });
    });
  }

  private __addRouter(routerAttr: RouterAttributes) {
    const { name, path, method, skipAuth, middlewares, handler } = routerAttr;
    const router: any = routerAttr.router;
    if (!Reflect.has(router, method)) {
      throw new Error(`'${method}' method is not allowed`);
    }
    if (!skipAuth) {
      middlewares.push(
        async (ctx: Context, next: () => void): Promise<void> => {
          if (!ctx.isAuthenticated()) {
            throw new Error("登录过期，请重新登录");
          }
          return next();
        }
      );
    }
    router[method](name, path, ...middlewares, handler);
    this._routerMap[name] = { ...routerAttr, path: router.route(name).path };
    this._logger.info(
      chalk.blueBright(`routed: ${method} ${router.route(name).path}`)
    );
  }

  /**
   * 查询路由信息列表
   * @param query 查询参数
   * @param pagination 分页参数
   * @returns 分页的路由信息列表
   */
  public getRouterList(
    query: Record<string, any>,
    pagination: Pagination
  ): EMAtttributesWithPagination<
    Omit<
      RouterAttributes,
      keyof { router: any; middlewares: any; handler: any }
    >
  > {
    const routers = Object.values(this._routerMap).filter((options: any) => {
      if (!query) {
        return true;
      }
      let res = true;
      Object.keys(query).forEach((key) => {
        res = options[key].indexOf(query[key]) > -1;
      });
      return res;
    });
    const routerDatas = routers.map((router) => {
      return _.pick(router, ["name", "desc", "path", "method", "skipAuth"]);
    });
    const routerss = _.chunk(routerDatas, pagination.pageSize);
    return {
      data: routerss[_.toInteger(pagination.current) - 1],
      pagination: { ...pagination, total: routers.length },
    };
  }

  /**
   * 通过路由名称获取路由信息
   * @param routerName 路由名称
   * @returns 路由信息
   */
  public getRouterByName(
    routerName: string
  ): Omit<
    RouterAttributes,
    keyof { router: any; middlewares: any; handler: any }
  > {
    const routerOptions = this._routerMap[routerName];
    return _.pick(routerOptions, [
      "name",
      "desc",
      "path",
      "method",
      "skipAuth",
    ]);
  }
}

export default new RouterService();
