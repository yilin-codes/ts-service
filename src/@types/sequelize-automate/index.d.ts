declare module "sequelize-automate" {
  class Automate {
    constructor(dbOptions: Automate.DbOptions, options: Automate.Options);
    /** get table definitions */
    getDefinitions(): Promise<Automate.Definition[]>;
    /** create model file by sequlize-automate plugin */
    run(): Promise<any>;
  }
  namespace Automate {
    export interface DbOptions {
      database: string;
      username: string;
      password: string;
      dialect: string;
      host: string;
      port: number;
      logging?: boolean;
      define?: Record<string, any>;
    }
    export interface Options {
      type: string;
      dir?: string;
      camelCase?: boolean;
      fileNameCamelCase?: boolean;
      typesDir?: string;
      emptyDir?: boolean;
      tables?: string[] | null;
      skipTables?: string[] | null;
      tsNoCheck?: boolean;
    }
    export interface Definition {
      modelName: string;
      modelFileName: string;
      tableName: string;
      attributes: Record<string, FieldAttr>;
    }
    export interface FieldAttr extends Record<string, any> {
      type: string;
      allowNull: boolean;
      defaultValue: any;
      primaryKey: boolean;
      autoIncrement: boolean;
      comment: string;
      field: string;
      unique?: string;
    }
  }
  export = Automate;
}
