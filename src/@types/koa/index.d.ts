import * as Koa from "koa";
import { Pagination } from "../../app/core/BaseService";

declare module "koa" {
  interface RouteParam {
    [key: string]: string;
  }
  interface QueryParam {
    query: Record<string, any>;
    pagination?: Pagination;
    options?: Record<string, any>;
    [key: string]: any;
  }
  interface DefaultContext {
    params: RouteParam;
    parsedQuery: QueryParam;
  }
}
