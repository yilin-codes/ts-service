import app from "./app";
import { apiRouter, appRouter, sysRouter } from "./app/core/router";
import { Log } from "./app/core/logs";
import RegisterControllers from "./app/controller/ControllerRegister";
import DbService from "./app/service/base/DbService";

app
  .use(appRouter.routes())
  .use(appRouter.allowedMethods())
  .use(apiRouter.routes())
  .use(apiRouter.allowedMethods())
  .use(sysRouter.routes())
  .use(sysRouter.allowedMethods());
app.listen(8888, async () => {
  Log(`NODE_ENV: ${process.env.NODE_ENV}`);
  await DbService.createDb();
  await DbService.syncDb();
  await DbService.initAdmin();
  RegisterControllers();
  Log("Server running on port 8888");
});
